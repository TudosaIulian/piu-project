﻿namespace ProjectPiu
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form5));
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.btnCirese = new System.Windows.Forms.Button();
            this.btnMenta = new System.Windows.Forms.Button();
            this.btnCiocolata = new System.Windows.Forms.Button();
            this.btnVanilie = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.BorderWidth = 3;
            this.rectangleShape1.CornerRadius = 8;
            this.rectangleShape1.Location = new System.Drawing.Point(335, 432);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(546, 123);
            // 
            // rectangleShape2
            // 
            this.rectangleShape2.BorderWidth = 3;
            this.rectangleShape2.CornerRadius = 8;
            this.rectangleShape2.Location = new System.Drawing.Point(352, 447);
            this.rectangleShape2.Name = "rectangleShape2";
            this.rectangleShape2.Size = new System.Drawing.Size(516, 94);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape2,
            this.rectangleShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(1203, 560);
            this.shapeContainer1.TabIndex = 2;
            this.shapeContainer1.TabStop = false;
            // 
            // btnCirese
            // 
            this.btnCirese.BackColor = System.Drawing.Color.Transparent;
            this.btnCirese.Location = new System.Drawing.Point(749, 464);
            this.btnCirese.Name = "btnCirese";
            this.btnCirese.Size = new System.Drawing.Size(97, 55);
            this.btnCirese.TabIndex = 18;
            this.btnCirese.Text = "Cirese";
            this.btnCirese.UseVisualStyleBackColor = false;
            this.btnCirese.Click += new System.EventHandler(this.btnCirese_Click);
            // 
            // btnMenta
            // 
            this.btnMenta.BackColor = System.Drawing.Color.Transparent;
            this.btnMenta.Location = new System.Drawing.Point(619, 464);
            this.btnMenta.Name = "btnMenta";
            this.btnMenta.Size = new System.Drawing.Size(97, 55);
            this.btnMenta.TabIndex = 17;
            this.btnMenta.Text = "Menta";
            this.btnMenta.UseVisualStyleBackColor = false;
            this.btnMenta.Click += new System.EventHandler(this.btnMenta_Click);
            // 
            // btnCiocolata
            // 
            this.btnCiocolata.BackColor = System.Drawing.Color.Transparent;
            this.btnCiocolata.Location = new System.Drawing.Point(491, 464);
            this.btnCiocolata.Name = "btnCiocolata";
            this.btnCiocolata.Size = new System.Drawing.Size(97, 55);
            this.btnCiocolata.TabIndex = 16;
            this.btnCiocolata.Text = "Ciocolata";
            this.btnCiocolata.UseVisualStyleBackColor = false;
            this.btnCiocolata.Click += new System.EventHandler(this.btnCiocolata_Click);
            // 
            // btnVanilie
            // 
            this.btnVanilie.BackColor = System.Drawing.Color.Transparent;
            this.btnVanilie.Location = new System.Drawing.Point(363, 464);
            this.btnVanilie.Name = "btnVanilie";
            this.btnVanilie.Size = new System.Drawing.Size(97, 55);
            this.btnVanilie.TabIndex = 15;
            this.btnVanilie.Text = "Vanilie";
            this.btnVanilie.UseVisualStyleBackColor = false;
            this.btnVanilie.Click += new System.EventHandler(this.btnVanilie_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DarkRed;
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 26.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.Green;
            this.label1.Location = new System.Drawing.Point(495, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(251, 43);
            this.label1.TabIndex = 14;
            this.label1.Text = "Ce aroma iti place?";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(372, 52);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(474, 374);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkRed;
            this.ClientSize = new System.Drawing.Size(1203, 560);
            this.Controls.Add(this.btnCirese);
            this.Controls.Add(this.btnMenta);
            this.Controls.Add(this.btnCiocolata);
            this.Controls.Add(this.btnVanilie);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.shapeContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form5";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Level3";
            this.Load += new System.EventHandler(this.Form5_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape2;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private System.Windows.Forms.Button btnCirese;
        private System.Windows.Forms.Button btnMenta;
        private System.Windows.Forms.Button btnCiocolata;
        private System.Windows.Forms.Button btnVanilie;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}