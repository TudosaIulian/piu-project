﻿namespace ProjectPiu
{
    partial class level1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(level1));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPrimavara = new System.Windows.Forms.Button();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.rectangleShape2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.btnVara = new System.Windows.Forms.Button();
            this.btnToamna = new System.Windows.Forms.Button();
            this.btnIarna = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(398, 71);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(474, 374);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DarkRed;
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 26.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.Green;
            this.label1.Location = new System.Drawing.Point(451, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(355, 43);
            this.label1.TabIndex = 1;
            this.label1.Text = "In ce anotimp te-ai nascut?";
            // 
            // btnPrimavara
            // 
            this.btnPrimavara.BackColor = System.Drawing.Color.Transparent;
            this.btnPrimavara.Location = new System.Drawing.Point(389, 483);
            this.btnPrimavara.Name = "btnPrimavara";
            this.btnPrimavara.Size = new System.Drawing.Size(97, 55);
            this.btnPrimavara.TabIndex = 2;
            this.btnPrimavara.Text = "Primavara";
            this.btnPrimavara.UseVisualStyleBackColor = false;
            this.btnPrimavara.Click += new System.EventHandler(this.btnPrimavara_Click);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape2,
            this.rectangleShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(1219, 599);
            this.shapeContainer1.TabIndex = 3;
            this.shapeContainer1.TabStop = false;
            // 
            // rectangleShape2
            // 
            this.rectangleShape2.BorderWidth = 3;
            this.rectangleShape2.CornerRadius = 8;
            this.rectangleShape2.Location = new System.Drawing.Point(376, 462);
            this.rectangleShape2.Name = "rectangleShape2";
            this.rectangleShape2.Size = new System.Drawing.Size(516, 94);
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.BorderWidth = 3;
            this.rectangleShape1.CornerRadius = 8;
            this.rectangleShape1.Location = new System.Drawing.Point(361, 448);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(546, 123);
            // 
            // btnVara
            // 
            this.btnVara.BackColor = System.Drawing.Color.Transparent;
            this.btnVara.Location = new System.Drawing.Point(517, 483);
            this.btnVara.Name = "btnVara";
            this.btnVara.Size = new System.Drawing.Size(97, 55);
            this.btnVara.TabIndex = 4;
            this.btnVara.Text = "Vara";
            this.btnVara.UseVisualStyleBackColor = false;
            this.btnVara.Click += new System.EventHandler(this.btnVara_Click);
            // 
            // btnToamna
            // 
            this.btnToamna.BackColor = System.Drawing.Color.Transparent;
            this.btnToamna.Location = new System.Drawing.Point(645, 483);
            this.btnToamna.Name = "btnToamna";
            this.btnToamna.Size = new System.Drawing.Size(97, 55);
            this.btnToamna.TabIndex = 5;
            this.btnToamna.Text = "Toamna";
            this.btnToamna.UseVisualStyleBackColor = false;
            this.btnToamna.Click += new System.EventHandler(this.btnToamna_Click);
            // 
            // btnIarna
            // 
            this.btnIarna.BackColor = System.Drawing.Color.Transparent;
            this.btnIarna.Location = new System.Drawing.Point(775, 483);
            this.btnIarna.Name = "btnIarna";
            this.btnIarna.Size = new System.Drawing.Size(97, 55);
            this.btnIarna.TabIndex = 6;
            this.btnIarna.Text = "Iarna";
            this.btnIarna.UseVisualStyleBackColor = false;
            this.btnIarna.Click += new System.EventHandler(this.btnIarna_Click);
            // 
            // level1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkRed;
            this.ClientSize = new System.Drawing.Size(1219, 599);
            this.Controls.Add(this.btnIarna);
            this.Controls.Add(this.btnToamna);
            this.Controls.Add(this.btnVara);
            this.Controls.Add(this.btnPrimavara);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.shapeContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "level1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Level1";
            this.Load += new System.EventHandler(this.level1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPrimavara;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape2;
        private System.Windows.Forms.Button btnVara;
        private System.Windows.Forms.Button btnToamna;
        private System.Windows.Forms.Button btnIarna;
    }
}