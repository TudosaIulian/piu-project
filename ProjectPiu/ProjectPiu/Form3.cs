﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace ProjectPiu
{
    public partial class level1 : Form
    {
        Punctaj concurent = new Punctaj();
        Form4 Levels = new Form4();
        public SoundPlayer sunet; 
        public static int scorfinal;
        public level1()
        {
            InitializeComponent();
            sunet = new SoundPlayer("sound.wav");
        }

        private void btnPrimavara_Click(object sender, EventArgs e)
        {
            concurent.varianta1(1);
            //lblafiseazatemp.Text = concurent.toString();
            scorfinal += 1;
            sunet.Play();
            Levels.Show();
            this.Hide();
        }

        private void btnVara_Click(object sender, EventArgs e)
        {
            concurent.varianta2();
            concurent.PuncteAcumulate = 2;
            scorfinal += 2;
            sunet.Play();
            Levels.Show();
            this.Hide();
        }

        private void btnToamna_Click(object sender, EventArgs e)
        {
            concurent.varianta3();
            scorfinal += 3;
            sunet.Play();
            Levels.Show();
            this.Hide();
        }

        private void btnIarna_Click(object sender, EventArgs e)
        {
            concurent.varianta4();
            scorfinal += 4;
            sunet.Play();
            Levels.Show();
            this.Hide();
        }

        private void level1_Load(object sender, EventArgs e)
        {
            //version 2.0
        }
    }
}
