﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectPiu
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //Timer for loading bar
        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                rectangleShape2.Width += 2;
                if (rectangleShape2.Width >= 702)
                {
                    timer1.Stop();
                    Form2 frm = new Form2();
                    frm.Show();
                    this.Hide();
                }
            }
            catch(Exception)
            {return;}
            if(rectangleShape2.Width <150) //I use rectangleShape  instead tick , because this mode have less code and it's more optimized.
            {
                lblLoading.Text = "Loading data...";
            }
            else if (rectangleShape2.Width >= 150 && rectangleShape2.Width < 300)
            {
                lblLoading.Text = "Loading Files...";
            }
            else if (rectangleShape2.Width >= 300 && rectangleShape2.Width < 600)
            {
                lblLoading.Text = "Loading Images...";
            }
            else if (rectangleShape2.Width >= 600)
            {
                lblLoading.Text = "Loading Complete!";
            }
            else
            {
                //Keep this for version 2.0 ->Animation stuffs
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Keep this for version 2.0 -> Animation stuffs
        }
    }
}
