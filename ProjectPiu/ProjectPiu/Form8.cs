﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace ProjectPiu
{
    public partial class Form8 : Form
    {
        public SoundPlayer sunet; 
        public Form8()
        {
            InitializeComponent();
            sunet = new SoundPlayer("sound.wav");
        }

        private void Form8_Load(object sender, EventArgs e)
        {
            //Objcent from class TextCaracter ... used for show information about each characters.
            TextCaracter afisare = new TextCaracter();
            lblScor.Text = level1.scorfinal.ToString();
            Form2.contorMuzica = 1;
            
            if (level1.scorfinal < 7) //1
            {
                pictureBox1.Image = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\poze\Bravo.jpg");
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                lblTextCaractere.Text = afisare.Bravo();
            }
            else if(level1.scorfinal >=7 && level1.scorfinal <9)//2
            {
                pictureBox1.Image = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\poze\Mojo.png");
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                lblTextCaractere.Text = afisare.Mojo();
            }
            else if (level1.scorfinal >= 9 && level1.scorfinal < 11)//3
            {
                pictureBox1.Image = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\poze\coiotul.jpg");
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                lblTextCaractere.Text = afisare.Coiotul();
            }
            else if (level1.scorfinal >= 11 && level1.scorfinal < 12)//4
            {
                pictureBox1.Image = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\poze\branza.jpg");
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                lblTextCaractere.Text = afisare.Branza();
            }
            else if (level1.scorfinal >= 12 && level1.scorfinal < 13)//5
            {
                pictureBox1.Image = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\poze\buttercup.jpg");
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                lblTextCaractere.Text = afisare.ButterCup();
            }
            else if (level1.scorfinal >= 13 && level1.scorfinal < 14)//6
            {
                pictureBox1.Image = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\poze\curaj.jpg");
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                lblTextCaractere.Text = afisare.Curaj();
            }
            else if (level1.scorfinal >= 14 && level1.scorfinal < 15)//7
            {
                pictureBox1.Image = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\poze\Daphne.jpg");
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                lblTextCaractere.Text = afisare.Daphne();
            }
            else if (level1.scorfinal >= 15 && level1.scorfinal < 16)//8
            {
                pictureBox1.Image = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\poze\DeeDee.jpg");
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                lblTextCaractere.Text = afisare.DeeDee();
            }
            else if (level1.scorfinal >= 16 && level1.scorfinal <17)//9
            {
                pictureBox1.Image = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\poze\Jimmy.png");
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                lblTextCaractere.Text = afisare.Jimmy();
            }
            else if (level1.scorfinal >= 17 && level1.scorfinal < 18)//10
            {
                pictureBox1.Image = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\poze\Scoutmasterlumpus.png");
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                lblTextCaractere.Text = afisare.ScoutMasterLumpus();
            }
            else if (level1.scorfinal >= 18 && level1.scorfinal < 19)//11
            {
                pictureBox1.Image = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\poze\tweety.png");
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                lblTextCaractere.Text = afisare.Tweety();
            }
            else //12
            {
                pictureBox1.Image = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\poze\Bugs_Bunny.jpg");
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                lblTextCaractere.Text = afisare.Bugs();
            }

            level1.scorfinal = 0;
            lblScor.Hide();
        }

        private void btnAfiseazaScor_Click(object sender, EventArgs e)
        {
            lblScor.Show();
            sunet.Play();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();          
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Form2 frm = new Form2();
            Form2.contorMuzica = 1;
            Form2 frm= new Form2(2);
            frm.Show();
            this.Hide();
        }
    }
}
