﻿namespace ProjectPiu
{
    partial class Form6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form6));
            this.rectangleShape2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.btnGatit = new System.Windows.Forms.Button();
            this.btnMiscare = new System.Windows.Forms.Button();
            this.btnPlimbare = new System.Windows.Forms.Button();
            this.btnCitit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // rectangleShape2
            // 
            this.rectangleShape2.BorderWidth = 3;
            this.rectangleShape2.CornerRadius = 8;
            this.rectangleShape2.Location = new System.Drawing.Point(334, 442);
            this.rectangleShape2.Name = "rectangleShape2";
            this.rectangleShape2.Size = new System.Drawing.Size(516, 94);
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.BorderWidth = 3;
            this.rectangleShape1.CornerRadius = 8;
            this.rectangleShape1.Location = new System.Drawing.Point(317, 427);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(546, 123);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape1,
            this.rectangleShape2});
            this.shapeContainer1.Size = new System.Drawing.Size(1203, 560);
            this.shapeContainer1.TabIndex = 0;
            this.shapeContainer1.TabStop = false;
            // 
            // btnGatit
            // 
            this.btnGatit.BackColor = System.Drawing.Color.Transparent;
            this.btnGatit.Location = new System.Drawing.Point(737, 463);
            this.btnGatit.Name = "btnGatit";
            this.btnGatit.Size = new System.Drawing.Size(97, 55);
            this.btnGatit.TabIndex = 24;
            this.btnGatit.Text = "Gatitul";
            this.btnGatit.UseVisualStyleBackColor = false;
            this.btnGatit.Click += new System.EventHandler(this.btnGatit_Click);
            // 
            // btnMiscare
            // 
            this.btnMiscare.BackColor = System.Drawing.Color.Transparent;
            this.btnMiscare.Location = new System.Drawing.Point(607, 463);
            this.btnMiscare.Name = "btnMiscare";
            this.btnMiscare.Size = new System.Drawing.Size(97, 55);
            this.btnMiscare.TabIndex = 23;
            this.btnMiscare.Text = "Sporturile";
            this.btnMiscare.UseVisualStyleBackColor = false;
            this.btnMiscare.Click += new System.EventHandler(this.btnMiscare_Click);
            // 
            // btnPlimbare
            // 
            this.btnPlimbare.BackColor = System.Drawing.Color.Transparent;
            this.btnPlimbare.Location = new System.Drawing.Point(479, 463);
            this.btnPlimbare.Name = "btnPlimbare";
            this.btnPlimbare.Size = new System.Drawing.Size(97, 55);
            this.btnPlimbare.TabIndex = 22;
            this.btnPlimbare.Text = "Plimbarea";
            this.btnPlimbare.UseVisualStyleBackColor = false;
            this.btnPlimbare.Click += new System.EventHandler(this.btnPlimbare_Click);
            // 
            // btnCitit
            // 
            this.btnCitit.BackColor = System.Drawing.Color.Transparent;
            this.btnCitit.Location = new System.Drawing.Point(351, 463);
            this.btnCitit.Name = "btnCitit";
            this.btnCitit.Size = new System.Drawing.Size(97, 55);
            this.btnCitit.TabIndex = 21;
            this.btnCitit.Text = "Cititul";
            this.btnCitit.UseVisualStyleBackColor = false;
            this.btnCitit.Click += new System.EventHandler(this.btnCitit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DarkRed;
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 26.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.Green;
            this.label1.Location = new System.Drawing.Point(432, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(336, 43);
            this.label1.TabIndex = 20;
            this.label1.Text = "Ce te relaxeaza mai mult?";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(317, 51);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(547, 369);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // Form6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkRed;
            this.ClientSize = new System.Drawing.Size(1203, 560);
            this.Controls.Add(this.btnGatit);
            this.Controls.Add(this.btnMiscare);
            this.Controls.Add(this.btnPlimbare);
            this.Controls.Add(this.btnCitit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.shapeContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form6";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Level4";
            this.Load += new System.EventHandler(this.Form6_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape2;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private System.Windows.Forms.Button btnGatit;
        private System.Windows.Forms.Button btnMiscare;
        private System.Windows.Forms.Button btnPlimbare;
        private System.Windows.Forms.Button btnCitit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;

    }
}