﻿namespace ProjectPiu
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form4));
            this.rectangleShape2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.btnIarna = new System.Windows.Forms.Button();
            this.btnToamna = new System.Windows.Forms.Button();
            this.btnVara = new System.Windows.Forms.Button();
            this.btnSangivn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // rectangleShape2
            // 
            this.rectangleShape2.BorderWidth = 3;
            this.rectangleShape2.CornerRadius = 8;
            this.rectangleShape2.Location = new System.Drawing.Point(368, 446);
            this.rectangleShape2.Name = "rectangleShape2";
            this.rectangleShape2.Size = new System.Drawing.Size(516, 94);
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.BorderWidth = 3;
            this.rectangleShape1.CornerRadius = 8;
            this.rectangleShape1.Location = new System.Drawing.Point(353, 432);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(546, 123);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape1,
            this.rectangleShape2});
            this.shapeContainer1.Size = new System.Drawing.Size(1219, 599);
            this.shapeContainer1.TabIndex = 1;
            this.shapeContainer1.TabStop = false;
            // 
            // btnIarna
            // 
            this.btnIarna.BackColor = System.Drawing.Color.Transparent;
            this.btnIarna.Location = new System.Drawing.Point(769, 466);
            this.btnIarna.Name = "btnIarna";
            this.btnIarna.Size = new System.Drawing.Size(97, 55);
            this.btnIarna.TabIndex = 12;
            this.btnIarna.Text = "Coleric";
            this.btnIarna.UseVisualStyleBackColor = false;
            this.btnIarna.Click += new System.EventHandler(this.btnIarna_Click);
            // 
            // btnToamna
            // 
            this.btnToamna.BackColor = System.Drawing.Color.Transparent;
            this.btnToamna.Location = new System.Drawing.Point(639, 466);
            this.btnToamna.Name = "btnToamna";
            this.btnToamna.Size = new System.Drawing.Size(97, 55);
            this.btnToamna.TabIndex = 11;
            this.btnToamna.Text = "Melancolic";
            this.btnToamna.UseVisualStyleBackColor = false;
            this.btnToamna.Click += new System.EventHandler(this.btnToamna_Click);
            // 
            // btnVara
            // 
            this.btnVara.BackColor = System.Drawing.Color.Transparent;
            this.btnVara.Location = new System.Drawing.Point(511, 466);
            this.btnVara.Name = "btnVara";
            this.btnVara.Size = new System.Drawing.Size(97, 55);
            this.btnVara.TabIndex = 10;
            this.btnVara.Text = "Flegmatic";
            this.btnVara.UseVisualStyleBackColor = false;
            this.btnVara.Click += new System.EventHandler(this.btnVara_Click);
            // 
            // btnSangivn
            // 
            this.btnSangivn.BackColor = System.Drawing.Color.Transparent;
            this.btnSangivn.Location = new System.Drawing.Point(383, 466);
            this.btnSangivn.Name = "btnSangivn";
            this.btnSangivn.Size = new System.Drawing.Size(97, 55);
            this.btnSangivn.TabIndex = 9;
            this.btnSangivn.Text = "Sangvin";
            this.btnSangivn.UseVisualStyleBackColor = false;
            this.btnSangivn.Click += new System.EventHandler(this.btnPrimavara_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DarkRed;
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 26.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.Green;
            this.label1.Location = new System.Drawing.Point(469, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(336, 43);
            this.label1.TabIndex = 8;
            this.label1.Text = "Ce tip de temperament ai?";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(392, 54);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(474, 374);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkRed;
            this.ClientSize = new System.Drawing.Size(1219, 599);
            this.Controls.Add(this.btnIarna);
            this.Controls.Add(this.btnToamna);
            this.Controls.Add(this.btnVara);
            this.Controls.Add(this.btnSangivn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.shapeContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form4";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Level2";
            this.Load += new System.EventHandler(this.Form4_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape2;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private System.Windows.Forms.Button btnIarna;
        private System.Windows.Forms.Button btnToamna;
        private System.Windows.Forms.Button btnVara;
        private System.Windows.Forms.Button btnSangivn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}