﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;
using System.Media;

namespace ProjectPiu
{
    public partial class Form2 : Form
    {
        //Using Windows Media Player for music and create an object for this an use it in Constructor, also using Media for sounds.
        WindowsMediaPlayer music = new WindowsMediaPlayer();
        public SoundPlayer sunet;
        public string numeTemp="";
        public static int contorMuzica;
        public Form2()
        {
            InitializeComponent();
            music.URL = "askfor.mp3";
            sunet = new SoundPlayer("sound.wav");
            
        }
        //This is a second Constructor for when user want to restart the app.
        public Form2(int x)
        {
            InitializeComponent();
            sunet = new SoundPlayer("sound.wav");
            numeTemp = txtBoxLogin.Text;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sunet.Play();
            Application.Exit();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            if (level1.scorfinal != 0)
            {
                music.controls.play();
            }
           
            btnLogin.Focus();
            if (contorMuzica == 1)
            {
                btnStartGame.Enabled = true;
                lblNameTop.Text = numeTemp;
            }
            btnStartGame.Enabled = false;
            txtBoxLogin.Hide();
            lblLogin.Hide();
            lblNameTop.Hide();
            btnDone.Hide();
            if (numeTemp.Length > 2)
            {
                lblNameTop.Text = "Don't work"; //Here i have a bug... second time don't show the name of user.
            }

        }

        private void oFFToolStripMenuItem_Click(object sender, EventArgs e)
        {            
                music.controls.stop();
           
        }

        private void oNToolStripMenuItem_Click(object sender, EventArgs e)
        {
            music.controls.play();
        }

        private void oNToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            sunet.Play();
        }

        private void oFFToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            sunet.Stop();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sunet.Play();
            MessageBox.Show("This app is created by Tudosa Iulian.\n Email:iulian.tudosa@student.usv.ro \n WebSite: www.stud.usv.ro/~iutudosa \n ©All Rights Rezerved.");
        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sunet.Play();
            lblNameTop.Text = " "; music.controls.stop();
            //MessageBox.Show("Resetare Joc, te rog introdu din nou un nume!");
            Form1 start = new Form1();
            start.Show(); this.Hide();
            level1.scorfinal = 0;
        }

        private void charactersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sunet.Play();
            Form9 caractere= new Form9();
            caractere.Show();
           // this.Hide();
           
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (contorMuzica == 0)
            {
                sunet.Play();
                txtBoxLogin.Show();
                lblLogin.Show();
                lblNameTop.Show();
                btnDone.Show();
                lblNumeRestart.Hide();
            }
            else
            {
                lblNameTop.Show();
                lblNumeRestart.Show();
                lblNumeRestart.Text = numeTemp;

                lblNameTop.Show(); lblNameTop.Text = txtBoxLogin.Text;
                btnStartGame.Enabled = true;

            }

        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            //verification if the name length have minimum 4 characters.
            if (txtBoxLogin.TextLength > 3)
            {
                sunet.Play();
                lblNameTop.Text = txtBoxLogin.Text;
                numeTemp = txtBoxLogin.Text;
                btnStartGame.Enabled = true;
            }
            else
            {
                MessageBox.Show("Your name length must to have minimum 4 characters","Avertisment",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
        }

        private void btnStartGame_Click(object sender, EventArgs e)
        {
            sunet.Play();
            level1 startGame = new level1();
            startGame.Show();            
            this.Hide();
        }

        private void level1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sunet.Play();
            MessageBox.Show("Acces restricted, soon on version 2.0 !" ,"Only for Administration",     MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void level2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sunet.Play();
            MessageBox.Show("Acces restricted, soon on version 2.0 !", "Only for Administration", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void level3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sunet.Play();
            MessageBox.Show("Acces restricted, soon on version 2.0 !", "Only for Administration", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void level4ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sunet.Play();
            MessageBox.Show("Acces restricted, soon on version 2.0 !", "Only for Administration", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void level5ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sunet.Play();
            MessageBox.Show("Acces restricted, soon on version 2.0!", "Only for Administration", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
