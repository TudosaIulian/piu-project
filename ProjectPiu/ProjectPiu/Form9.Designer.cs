﻿namespace ProjectPiu
{
    partial class Form9
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.bravoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jhonnyBravoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bugsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttercupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mojoJojoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tweetyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.lumpusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.deeDeeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.jimmyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.cotiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.cheeseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.stupidDogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.daphneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bravoToolStripMenuItem,
            this.bugsToolStripMenuItem,
            this.buttercupToolStripMenuItem,
            this.mojoJojoToolStripMenuItem,
            this.tweetyToolStripMenuItem,
            this.lumpusToolStripMenuItem,
            this.deeDeeToolStripMenuItem,
            this.jimmyToolStripMenuItem,
            this.cotiToolStripMenuItem,
            this.cheeseToolStripMenuItem,
            this.stupidDogToolStripMenuItem,
            this.daphneToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1363, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // bravoToolStripMenuItem
            // 
            this.bravoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.jhonnyBravoToolStripMenuItem});
            this.bravoToolStripMenuItem.Name = "bravoToolStripMenuItem";
            this.bravoToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.bravoToolStripMenuItem.Text = "Bugs Bunny";
            this.bravoToolStripMenuItem.Click += new System.EventHandler(this.bravoToolStripMenuItem_Click);
            // 
            // jhonnyBravoToolStripMenuItem
            // 
            this.jhonnyBravoToolStripMenuItem.Name = "jhonnyBravoToolStripMenuItem";
            this.jhonnyBravoToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.jhonnyBravoToolStripMenuItem.Text = "Show";
            this.jhonnyBravoToolStripMenuItem.Click += new System.EventHandler(this.jhonnyBravoToolStripMenuItem_Click);
            // 
            // bugsToolStripMenuItem
            // 
            this.bugsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bToolStripMenuItem});
            this.bugsToolStripMenuItem.Name = "bugsToolStripMenuItem";
            this.bugsToolStripMenuItem.Size = new System.Drawing.Size(90, 20);
            this.bugsToolStripMenuItem.Text = "Johnny Bravo";
            this.bugsToolStripMenuItem.Click += new System.EventHandler(this.bugsToolStripMenuItem_Click);
            // 
            // bToolStripMenuItem
            // 
            this.bToolStripMenuItem.Name = "bToolStripMenuItem";
            this.bToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.bToolStripMenuItem.Text = "Show";
            this.bToolStripMenuItem.Click += new System.EventHandler(this.bToolStripMenuItem_Click);
            // 
            // buttercupToolStripMenuItem
            // 
            this.buttercupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem});
            this.buttercupToolStripMenuItem.Name = "buttercupToolStripMenuItem";
            this.buttercupToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.buttercupToolStripMenuItem.Text = "Buttercup";
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.showToolStripMenuItem.Text = "Show";
            this.showToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
            // 
            // mojoJojoToolStripMenuItem
            // 
            this.mojoJojoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem1});
            this.mojoJojoToolStripMenuItem.Name = "mojoJojoToolStripMenuItem";
            this.mojoJojoToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.mojoJojoToolStripMenuItem.Text = "MojoJojo";
            // 
            // showToolStripMenuItem1
            // 
            this.showToolStripMenuItem1.Name = "showToolStripMenuItem1";
            this.showToolStripMenuItem1.Size = new System.Drawing.Size(103, 22);
            this.showToolStripMenuItem1.Text = "Show";
            this.showToolStripMenuItem1.Click += new System.EventHandler(this.showToolStripMenuItem1_Click);
            // 
            // tweetyToolStripMenuItem
            // 
            this.tweetyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem2});
            this.tweetyToolStripMenuItem.Name = "tweetyToolStripMenuItem";
            this.tweetyToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.tweetyToolStripMenuItem.Text = "Tweety";
            // 
            // showToolStripMenuItem2
            // 
            this.showToolStripMenuItem2.Name = "showToolStripMenuItem2";
            this.showToolStripMenuItem2.Size = new System.Drawing.Size(103, 22);
            this.showToolStripMenuItem2.Text = "Show";
            this.showToolStripMenuItem2.Click += new System.EventHandler(this.showToolStripMenuItem2_Click);
            // 
            // lumpusToolStripMenuItem
            // 
            this.lumpusToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem3});
            this.lumpusToolStripMenuItem.Name = "lumpusToolStripMenuItem";
            this.lumpusToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.lumpusToolStripMenuItem.Text = "Lumpus";
            this.lumpusToolStripMenuItem.Click += new System.EventHandler(this.lumpusToolStripMenuItem_Click);
            // 
            // showToolStripMenuItem3
            // 
            this.showToolStripMenuItem3.Name = "showToolStripMenuItem3";
            this.showToolStripMenuItem3.Size = new System.Drawing.Size(103, 22);
            this.showToolStripMenuItem3.Text = "Show";
            this.showToolStripMenuItem3.Click += new System.EventHandler(this.showToolStripMenuItem3_Click);
            // 
            // deeDeeToolStripMenuItem
            // 
            this.deeDeeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem4});
            this.deeDeeToolStripMenuItem.Name = "deeDeeToolStripMenuItem";
            this.deeDeeToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.deeDeeToolStripMenuItem.Text = "DeeDee";
            // 
            // showToolStripMenuItem4
            // 
            this.showToolStripMenuItem4.Name = "showToolStripMenuItem4";
            this.showToolStripMenuItem4.Size = new System.Drawing.Size(103, 22);
            this.showToolStripMenuItem4.Text = "Show";
            this.showToolStripMenuItem4.Click += new System.EventHandler(this.showToolStripMenuItem4_Click);
            // 
            // jimmyToolStripMenuItem
            // 
            this.jimmyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem5});
            this.jimmyToolStripMenuItem.Name = "jimmyToolStripMenuItem";
            this.jimmyToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.jimmyToolStripMenuItem.Text = "Jimmy";
            // 
            // showToolStripMenuItem5
            // 
            this.showToolStripMenuItem5.Name = "showToolStripMenuItem5";
            this.showToolStripMenuItem5.Size = new System.Drawing.Size(103, 22);
            this.showToolStripMenuItem5.Text = "Show";
            this.showToolStripMenuItem5.Click += new System.EventHandler(this.showToolStripMenuItem5_Click);
            // 
            // cotiToolStripMenuItem
            // 
            this.cotiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem6});
            this.cotiToolStripMenuItem.Name = "cotiToolStripMenuItem";
            this.cotiToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.cotiToolStripMenuItem.Text = "Coyote";
            // 
            // showToolStripMenuItem6
            // 
            this.showToolStripMenuItem6.Name = "showToolStripMenuItem6";
            this.showToolStripMenuItem6.Size = new System.Drawing.Size(103, 22);
            this.showToolStripMenuItem6.Text = "Show";
            this.showToolStripMenuItem6.Click += new System.EventHandler(this.showToolStripMenuItem6_Click);
            // 
            // cheeseToolStripMenuItem
            // 
            this.cheeseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem7});
            this.cheeseToolStripMenuItem.Name = "cheeseToolStripMenuItem";
            this.cheeseToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.cheeseToolStripMenuItem.Text = "Cheese";
            // 
            // showToolStripMenuItem7
            // 
            this.showToolStripMenuItem7.Name = "showToolStripMenuItem7";
            this.showToolStripMenuItem7.Size = new System.Drawing.Size(103, 22);
            this.showToolStripMenuItem7.Text = "Show";
            this.showToolStripMenuItem7.Click += new System.EventHandler(this.showToolStripMenuItem7_Click);
            // 
            // stupidDogToolStripMenuItem
            // 
            this.stupidDogToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem8});
            this.stupidDogToolStripMenuItem.Name = "stupidDogToolStripMenuItem";
            this.stupidDogToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.stupidDogToolStripMenuItem.Text = "Stupid Dog";
            // 
            // showToolStripMenuItem8
            // 
            this.showToolStripMenuItem8.Name = "showToolStripMenuItem8";
            this.showToolStripMenuItem8.Size = new System.Drawing.Size(103, 22);
            this.showToolStripMenuItem8.Text = "Show";
            this.showToolStripMenuItem8.Click += new System.EventHandler(this.showToolStripMenuItem8_Click);
            // 
            // daphneToolStripMenuItem
            // 
            this.daphneToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem9});
            this.daphneToolStripMenuItem.Name = "daphneToolStripMenuItem";
            this.daphneToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.daphneToolStripMenuItem.Text = "Daphne";
            // 
            // showToolStripMenuItem9
            // 
            this.showToolStripMenuItem9.Name = "showToolStripMenuItem9";
            this.showToolStripMenuItem9.Size = new System.Drawing.Size(103, 22);
            this.showToolStripMenuItem9.Text = "Show";
            this.showToolStripMenuItem9.Click += new System.EventHandler(this.showToolStripMenuItem9_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(355, 64);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(718, 417);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(589, 516);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(257, 57);
            this.btnBack.TabIndex = 2;
            this.btnBack.Text = "Back to the App";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // Form9
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkRed;
            this.ClientSize = new System.Drawing.Size(1363, 645);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form9";
            this.Text = "Caractere";
            this.Load += new System.EventHandler(this.Form9_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem bravoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jhonnyBravoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bugsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buttercupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mojoJojoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tweetyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lumpusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deeDeeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jimmyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cotiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cheeseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stupidDogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem daphneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem9;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnBack;
    }
}