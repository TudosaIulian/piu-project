﻿namespace ProjectPiu
{
    partial class Form8
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form8));
            this.lblScor = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAfiseazaScor = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.lblTextCaractere = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblScor
            // 
            this.lblScor.AutoSize = true;
            this.lblScor.Font = new System.Drawing.Font("Monotype Corsiva", 24F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblScor.ForeColor = System.Drawing.Color.White;
            this.lblScor.Location = new System.Drawing.Point(1096, 523);
            this.lblScor.Name = "lblScor";
            this.lblScor.Size = new System.Drawing.Size(127, 39);
            this.lblScor.TabIndex = 3;
            this.lblScor.Text = "Scor final";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(293, 63);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(706, 429);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 36F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(374, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(508, 57);
            this.label1.TabIndex = 21;
            this.label1.Text = "Wow ce personaj Interesant!";
            // 
            // btnAfiseazaScor
            // 
            this.btnAfiseazaScor.BackColor = System.Drawing.Color.DarkRed;
            this.btnAfiseazaScor.ForeColor = System.Drawing.Color.White;
            this.btnAfiseazaScor.Location = new System.Drawing.Point(1059, 411);
            this.btnAfiseazaScor.Name = "btnAfiseazaScor";
            this.btnAfiseazaScor.Size = new System.Drawing.Size(127, 81);
            this.btnAfiseazaScor.TabIndex = 22;
            this.btnAfiseazaScor.Text = "Apasa Aici pentru Scor!";
            this.btnAfiseazaScor.UseVisualStyleBackColor = false;
            this.btnAfiseazaScor.Click += new System.EventHandler(this.btnAfiseazaScor_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1059, 199);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(127, 70);
            this.button1.TabIndex = 23;
            this.button1.Text = "Home";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1059, 304);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(127, 70);
            this.button2.TabIndex = 24;
            this.button2.Text = "Exit";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // lblTextCaractere
            // 
            this.lblTextCaractere.AutoSize = true;
            this.lblTextCaractere.BackColor = System.Drawing.Color.DarkRed;
            this.lblTextCaractere.Font = new System.Drawing.Font("Monotype Corsiva", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblTextCaractere.ForeColor = System.Drawing.Color.White;
            this.lblTextCaractere.Location = new System.Drawing.Point(12, 112);
            this.lblTextCaractere.Name = "lblTextCaractere";
            this.lblTextCaractere.Size = new System.Drawing.Size(51, 22);
            this.lblTextCaractere.TabIndex = 25;
            this.lblTextCaractere.Text = "Wow";
            // 
            // Form8
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkRed;
            this.ClientSize = new System.Drawing.Size(1237, 568);
            this.Controls.Add(this.lblTextCaractere);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnAfiseazaScor);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblScor);
            this.Name = "Form8";
            this.Text = "GameOver";
            this.Load += new System.EventHandler(this.Form8_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblScor;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAfiseazaScor;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label lblTextCaractere;
    }
}