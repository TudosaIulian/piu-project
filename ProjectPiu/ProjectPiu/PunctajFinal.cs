﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPiu
{
    class PunctajFinal
    {
        List<Punctaj> Final;
        public PunctajFinal()
        { 
           Final= new List<Punctaj>();
        }
        public List<Punctaj> Lista
        {
            get
            {
                return Final;
            }
            set
            {
                Final = value;
            }

        }
        public void Rezultat(Punctaj p)
        {
            Lista.Add(p);
        }

    }
}
