﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ProjectPiu
{
    public partial class Form9 : Form
    {
        private ImageList imagelst;
        public Form9()
        {
            InitializeComponent();
            imagelst = new ImageList();
        }

        private void bugsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //fixed some bugs
        }

        private void bravoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //fixed some bugs
        }

        private void jhonnyBravoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = imagelst.Images["Bugs"];
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
           // pictureBox1.Image = MyNamespace.Properties.Resources.Bravo;
        }

        private void bToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = imagelst.Images["Bravo"];
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void Form9_Load(object sender, EventArgs e)
        {
            Image i = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\poze\Bravo.jpg");
            imagelst.Images.Add("Bravo",i);
            i = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\Resources\Bugs_Bunny.jpg");
            imagelst.Images.Add("Bugs",i);
            i = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\Resources\buttercup.jpg");
            imagelst.Images.Add("Buttercup", i);
            i = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\Resources\Mojo.png");
            imagelst.Images.Add("Mojo", i);
            i = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\Resources\tweety.png");
            imagelst.Images.Add("Tweety", i);
            i = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\Resources\Scoutmasterlumpus.png");
            imagelst.Images.Add("lumpus", i);
            i = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\Resources\DeeDee.jpg");
            imagelst.Images.Add("DeeDee", i);
            i = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\Resources\Jimmy.png");
            imagelst.Images.Add("Jimmy", i);
            i = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\Resources\coiotul.jpg");
            imagelst.Images.Add("Coiotul", i);
            i = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\Resources\branza.jpg");
            imagelst.Images.Add("branza", i);
            i = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\Resources\curaj.jpg");
            imagelst.Images.Add("curaj", i);
            i = new Bitmap(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\Resources\Daphne.jpg");
            imagelst.Images.Add("Daphne", i);



            //version 2.0 -> create an array
            //this.AutoScroll = true;

            //string[] list = Directory.GetFiles(@"F:\scoala anul 3\piu\teme\lab11\PiuProject\project\ProjectPiu\ProjectPiu\poze", "*.jpg");
            //PictureBox[] picturebox = new PictureBox[list.Length];
            //int y = 0;
            //for (int index = 0; index < picturebox.Length; index++)
            //{
            //    this.Controls.Add(picturebox[index]);
            //    // Following three lines set the images(picture boxes) locations
            //    if (index % 3 == 0)
            //        y = y + 150; // 3 images per rows, first image will be at (20,150)
            //    picturebox[index].Location = new Point(index * 120 + 20, y);

            //    picturebox[index].Size = new Size(100, 120);
            //    picturebox[index].Image = Image.FromFile(list[index]);
            //}
        }

        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = imagelst.Images["Buttercup"];
        }

        private void showToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = imagelst.Images["Mojo"];
        }

        private void showToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = imagelst.Images["Tweety"];
        }

        private void lumpusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //optimized.
        }

        private void showToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = imagelst.Images["lumpus"];
        }

        private void showToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = imagelst.Images["DeeDee"];
        }

        private void showToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = imagelst.Images["Jimmy"];
        }

        private void showToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = imagelst.Images["coiotul"];
        }

        private void showToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = imagelst.Images["branza"];
        }

        private void showToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = imagelst.Images["curaj"];
        }

        private void showToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = imagelst.Images["Daphne"];
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
