﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace ProjectPiu
{
    public partial class Form4 : Form
    {
        Punctaj concurent2 = new Punctaj();
        PunctajFinal final;
        Form5 level2 = new Form5();
        public SoundPlayer sunet; 
        public Form4()
        {
            InitializeComponent();
            final =   new PunctajFinal();
            sunet = new SoundPlayer("sound.wav");
        }

        private void Form4_Load(object sender, EventArgs e)
        {
           // lblScor.Text = Form3.scorfinal;
        }

        private void btnAfiseaza_Click(object sender, EventArgs e)
        {
            //lblScor.Text = final.Rezultat(concurent2.toString());
        }

        private void btnPrimavara_Click(object sender, EventArgs e)
        {
            level1.scorfinal += 1;
            sunet.Play();
            level2.Show();
            this.Hide();
        }

        private void btnVara_Click(object sender, EventArgs e)
        {
            level1.scorfinal += 2;
            sunet.Play();
            level2.Show();
            this.Hide();
        }

        private void btnToamna_Click(object sender, EventArgs e)
        {
            level1.scorfinal += 3;
            sunet.Play();
            level2.Show();
            this.Hide();
        }

        private void btnIarna_Click(object sender, EventArgs e)
        {
            level1.scorfinal += 4;
            sunet.Play();
            level2.Show();
            this.Hide();
        }
    }
}
