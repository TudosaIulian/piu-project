﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace ProjectPiu
{
    public partial class Form6 : Form
    {
        Form7 level4 = new Form7();
        public SoundPlayer sunet; 
        public Form6()
        {
            InitializeComponent();
            sunet=new SoundPlayer("sound.wav");
        }

        private void Form6_Load(object sender, EventArgs e)
        {
           
        }

        private void btnCitit_Click(object sender, EventArgs e)
        {
            level1.scorfinal += 1;
            sunet.Play();
            level4.Show();
            this.Hide();
        }

        private void btnPlimbare_Click(object sender, EventArgs e)
        {
            level1.scorfinal += 2;
            sunet.Play();
            level4.Show();
            this.Hide();
        }

        private void btnMiscare_Click(object sender, EventArgs e)
        {
            level1.scorfinal += 3;
            sunet.Play();
            level4.Show();
            this.Hide();
        }

        private void btnGatit_Click(object sender, EventArgs e)
        {
            level1.scorfinal += 4;
            sunet.Play();
            level4.Show();
            this.Hide();
        }
    }
}
