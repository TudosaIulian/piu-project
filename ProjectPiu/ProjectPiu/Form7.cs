﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace ProjectPiu
{
    public partial class Form7 : Form
    {
        Form8 final = new Form8();
        public SoundPlayer sunet; 
        public Form7()
        {
            InitializeComponent();
            sunet= new SoundPlayer("sound.wav");
        }

        private void btnRock_Click(object sender, EventArgs e)
        {
            level1.scorfinal += 1;
            sunet.Play();
            final.Show();
            this.Hide();
        }

        private void btnPop_Click(object sender, EventArgs e)
        {
            level1.scorfinal += 2;
            sunet.Play();
            final.Show();
            this.Hide();
        }

        private void btnClasic_Click(object sender, EventArgs e)
        {
            level1.scorfinal += 3;
            sunet.Play();
            final.Show();
            this.Hide();
        }

        private void btnHipHop_Click(object sender, EventArgs e)
        {
            level1.scorfinal += 4;
            sunet.Play();
            final.Show();
            this.Hide();
        }
    }
}
