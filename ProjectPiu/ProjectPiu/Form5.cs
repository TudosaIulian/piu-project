﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace ProjectPiu
{
    public partial class Form5 : Form
    {
        Form6 level3 = new Form6();
        public SoundPlayer sunet; 
        public Form5()
        {
            InitializeComponent();
            sunet = new SoundPlayer("sound.wav");
        }

        private void Form5_Load(object sender, EventArgs e)
        {
           // lblScor.Text = Form3.scorfinal.ToString();
        }

        private void btnVanilie_Click(object sender, EventArgs e)
        {
            level1.scorfinal += 1;
            sunet.Play();
            level3.Show();
            this.Hide();
        }

        private void btnCiocolata_Click(object sender, EventArgs e)
        {
            level1.scorfinal += 2;
            sunet.Play();
            level3.Show();
            this.Hide();
        }

        private void btnMenta_Click(object sender, EventArgs e)
        {
            level1.scorfinal += 3;
            sunet.Play();
            level3.Show();
            this.Hide();
        }

        private void btnCirese_Click(object sender, EventArgs e)
        {
            level1.scorfinal += 4;
            sunet.Play();
            level3.Show();
            this.Hide();
        }
    }
}
