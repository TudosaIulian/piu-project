﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPiu
{
    public class TextCaracter
    {
        public string afisare()
        {

            return "\t  Wow ai o ambitie incredibila\nCeea ce descoperi este\n o forma noua a ta\n cu mult drag eu";
        }
        public string Bravo()
        {
            return "\t  Wow, esti super!\nTe mentii intr-o forma buna\n Keep up the good Work!\n ";
        }
        public string Mojo()
        {
            return "\t  Wow, intr-o alta viata\nAi fost criminal in serie\nincearca sa fii mai bun\n nu uita\nBinele invinge intotdeauna";
        }
        public string Coiotul()
        {
            return "\t  Wow, incearca sa\ndevii o persoana \n mai prietenoasa si sa\n fii mai deschis";
        }
        public string Branza()
        {
            return "\t  Wow, esti o persoana Comica\nIti place sa fii\n numai in mijlocul atentiei\n ";
        }
        public string ButterCup()
        {
            return "\t  Wow, esti o persoana puternica\nnimic nu te doboara\n cand te supara cineva\n nu mai pune la suflet";
        }
        public string Curaj()
        {
            return "\t  Wow, esti o persoana curajoasa\nAi face orice pentru\n o persoana apropiata\n mai ales daca este\n jumatatea ta in joc";
        }
        public string Daphne()
        {
            return "\t  Wow, esti o persoana cool\nIti place sa fii la moda\n stii sa te imbraci iar\n prietenii apreciaza asta";
        }
        public string DeeDee()
        {
            return "\t  Wow, esti o persoana energica\nNu iti place sa\nstai degeaba si cauti\nmereu ceva de facut";
        }
        public string Jimmy()
        {
            return "\t  Wow, esti o persoana creativa\nAi niste idei mari\n dar nu de fiecare data\nsunt cele mai bune";
        }
        public string ScoutMasterLumpus()
        {
            return "\t  Wow, esti o persoana hotarata\nIti place sa detii controlul\ndar de multe ori\nnu prea reusesti";
        }
        public string Tweety()
        {
            return "\t  Wow, esti o persoana simpatica\nToata lumea te adora\nDin bunatatea ta\nofera si la altii";
        }
        public string Bugs()
        {
            return "\t  Wow, esti o persoana amuzanta\nIti place sa faci glume\n iar prietenii apreciaza asta\ntine-o tot asa";
        }

    }
}
